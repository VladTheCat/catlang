local c = catlang
c.words = {
		["+"] = function()
			local a, b = c.getFromStack(0, 2)
			a, b = tonumber(a), tonumber(b)
			c.put(0, a + b)
		end,
		["-"] = function()
			local a, b = c.getFromStack(0, 2)
			a, b = tonumber(a), tonumber(b)
			c.put(0, a - b)
		end,
		["*"] = function()
			local a, b = c.getFromStack(0, 2)
			a, b = tonumber(a), tonumber(b)
			c.put(0, a * b)
		end,
		["/"] = function()
			local a, b = c.getFromStack(0, 2)
			a, b = tonumber(a), tonumber(b)
			c.put(0, b / a)
		end,
		["++"] = function()
			c.put(0, tonumber(c.getFromStack(0, 0)) + 1)
		end,
		["--"] = function()
			c.put(0, tonumber(c.getFromStack(0, 0)) - 1)
		end,
		["="] = function()
			local a, b = c.getFromStack(0, 2)
			if tonumber(a) and tonumber(b) then
				a = tonumber(a)
				b = tonumber(b)
			end
			if a == b then
				c.put(0, 1)
			else
				c.put(0, 0)
			end
		end,
		[">"] = function()
			local a, b = c.getFromStack(0, 2)
			a, b = tonumber(a), tonumber(b)
			if b > a then
				c.put(0, 1)
			else
				c.put(0, 0)
			end
		end,
		["<"] = function()
			local a, b = c.getFromStack(0, 2)
			a, b = tonumber(a), tonumber(b)
			if a > b then
				c.put(0, 1)
			else
				c.put(0, 0)
			end
		end,
		["not"] = function()
			local a = c.getFromStack(0, 0)
			a = tonumber(a)
			if a == 0 then a = 1 else a = 0 end
			c.put(0, a)
		end,
		["and"] = function()
			local a, b = c.getFromStack(0, 2)
			a, b = tonumber(a), tonumber(b)
			if a == 1 and b == 1 then
				c.put(0, 1)
			else
				c.put(0, 0)
			end
		end,
		["or"] = function()
			local a, b = c.getFromStack(0, 2)
			a, b = tonumber(a), tonumber(b)
			if a == 1 or b == 1 then
				c.put(0, 1)
			else
				c.put(0, 0)
			end
		end,
		["xor"] = function()
			local a, b = c.getFromStack(0, 2)
			a, b = tonumber(a), tonumber(b)
			if a ~= b then
				c.put(0, 1)
			else
				c.put(0, 0)
			end
		end,
		["if"] = function()
			local statement = c.getFromStack(0, 0)
			if statement == 0 then
				local n = c.currentWord
				local level = 0
				for _wordN = c.currentWord+1, #c.codeWords["MAIN"] do
					local cw = c.codeWords["MAIN"][_wordN]
					if cw == "end" then
						if level == 0 then
							n = _wordN
							break
						else
							level = level - 1
						end
					elseif cw == "if" then
						level = level + 1
					end
				end
				c.currentWord = n
			end
		end,
		["sin"] = function()
			local a = tonumber(c.getFromStack(0, 0))
			c.put(0, math.sin(math.rad(a)))
		end,
		["cos"] = function()
			local a = tonumber(c.getFromStack(0, 0))
			c.put(0, math.cos(math.rad(a)))
		end,
		["tan"] = function()
			local a = tonumber(c.getFromStack(0, 0))
			c.put(0, math.tan(math.rad(a)))
		end,
		["atan"] = function()
			local a = tonumber(c.getFromStack(0, 0))
			c.put(0, math.deg(math.atan(a)))
		end,
		["atan2"] = function()
			local a, b = c.getFromStack(0, 2)
			a, b = tonumber(a), tonumber(b)
			c.put(0, math.deg(math.atan2(b, a)))
		end,
		["abs"] = function()
			local a = tonumber(c.getFromStack(0, 0))
			c.put(0, math.abs(a))
		end,
		["max"] = function()
			local a, b = c.getFromStack(0, 2)
			a, b = tonumber(a), tonumber(b)
			c.put(0, math.max(a, b))
		end,
		["min"] = function()
			local a, b = c.getFromStack(0, 2)
			a, b = tonumber(a), tonumber(b)
			c.put(0, math.min(a, b))
		end,
		["pi"] = function()
			c.put(0, math.pi)
		end,
		["concat"] = function()
			local a, b = c.getFromStack(0, 2)
			c.put(0, tostring(b)..tostring(a))
		end,
		["div"] = function()
			local a, b = c.getFromStack(0, 2)
			c.put(0, math.floor(b / a))
		end,
		["mod"] = function()
			local a, b = c.getFromStack(0, 2)
			c.put(0, b % a)
		end,
		["rnd"] = function()
			local a, b = c.getFromStack(0, 2)
			a, b = tonumber(a), tonumber(b)
			--math.randomseed(os.clock() - START_TIME)
			local result = math.random() * (a - b) + b
			c.put(0, result)
		end,
		["floor"] = function()
			c.put(0, math.floor(c.getFromStack(0, 0)))
		end,
		["ceil"] = function()
			c.put(0, math.ceil(c.getFromStack(0, 0)))
		end,
		["out"] = function()
			io.write(c.getFromStack(0, 0))
		end,
		["outl"] = function()
			print(c.getFromStack(0, 0))
		end,
		["outd"] = function()
			io.write(c.stack[0][#c.stack[0]])
		end,
		["outdl"] = function()
			print(c.stack[0][#c.stack[0]])
		end,
		["line"] = function()
			io.write("\n")
		end,
		["get"] = function()
			c.put(0, io.read())
		end,
		["dub"] = function()
			c.put(0, c.stack[0][#c.stack[0]])
		end,
		["getstack"] = function()
			for i, v in ipairs(c.stack[0]) do
				print("i: "..i.." = "..v)
			end
		end,
		["words"] = function( ... )
			print("SYSTEM WORDS: ")
			for i, v in pairs(c.words) do
				io.write(i.." ; ")
			end
			print("\nUSER WORDS: ")
			for i, v in pairs(c.codeWords) do
				if i ~= "MAIN" then
					io.write(i.." ; ")
				end
			end
		end,
		["wait"] = function()
			catlang.sleep(c.getFromStack(0, 0))
		end,
		["mark"] = function()
			c.lables[c.codeWords[_CODE_WORDS][c.currentWord+1]] = c.currentWord + 1
		end,
		["jump"] = function()
			c.currentWord = c.lables[c.codeWords[_CODE_WORDS][c.currentWord+1]]
		end,
		["pause"] = function()
			io.write("PRESS ENTER TO CONTINUE...") io.read()
		end,
		["halt"] = function()
			c.execute = false
		end
	}