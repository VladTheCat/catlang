# Catlang #

Author: [VladTheCat](http://vk.com/dev.vladthecat)
Language: Lua

Simple stack-oriented scripting language. You can include it to your Lua project.

### How to setup ###

* Download sources or clone repository
* Place it to your project folder
* Place ' require "catlang" ' and "catlang.initialize()"  in top of main project file
* Place it where you need to launch catlang code
```
catlang.load("test.catlang")
catlang.run()
```
* Enjoy!

### Words ###
* +, -, *, /, div, mod - algebraic operators
* concat - concatenate strings (or numbers, converted to strings)
* out, outl - print value from top of stack
* outd, outdl - print value without remove it from stack
* get - wait value from user
* dub - dublicate value on top of stack
* mark - set label for jumps
* jump - unconditional jump to label (goto start;)
* wait - pause for N (got from stack) seconds
* pause - program stopped while you don't pressed Enter
* halt - stop running