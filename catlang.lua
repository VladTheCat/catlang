START_TIME = os.time()
math.randomseed(os.time())

for i = 0, 100000 do
	math.random()
end

catlang = {}
require "catwords"
--require( "iuplua" )

local c = catlang

function catlang.sleep(n)  -- seconds
	local t0 = os.clock()
	while os.clock() - t0 <= n do end
end

function catlang.initialize()
	c.codeWords = {}
	c.codeWords["MAIN"] = {}
	c.lables = {}
	c.variables = {}
	c.stack = {}
	c.tempString = ""
	c.writeString = false
	for i = 0, 255 do
		c.stack[i] = {}
	end
	c.currentWord = 1
	c.execute = true
end

function catlang.load(file)
	local f = io.open(file, "r")
	if f then
		for line in f:lines() do
			local startChar = 1
			local tempWord = ""
			local schar = string.sub(line, 1, 1)
			if schar ~= "!" then
				for j = 1, #line do
					local char = string.sub(line, j, j)
					if schar == "'" then
						if not(char == "\n" or char == ";") then
							tempWord = tempWord .. char
						elseif tempWord and tempWord ~= "" then
							table.insert(c.codeWords["MAIN"], tempWord)
							--print(tempWord)
							tempWord = ""
						end
					else
						if not(char == " " or char == "\n" or char == "\0" or char == ";") then
							tempWord = tempWord .. char
						elseif tempWord ~= "" then
							table.insert(c.codeWords["MAIN"], tempWord)
							--print(tempWord)
							tempWord = ""
						end
					end
				end
			end
		end
		f:close()
		return true
	end
	return false
end


function catlang.parse(codeWords)
	while c.execute do
		_CODE_WORDS = codeWords
		local word = c.codeWords[codeWords][c.currentWord]
		local schar = string.sub(word, 1, 1)
		if tonumber(word) then
			table.insert(c.stack[0], tonumber(word))
		elseif schar == "'" then
			table.insert(c.stack[0], string.sub(word, 2))
		elseif schar == "$" then
			c.variables[string.sub(word, 2)] = c.getFromStack(0, 0)
		elseif schar == "&" then
			c.put(0, c.variables[string.sub(word, 2)])
		elseif c.words[word] then
			c.words[word]()
		else
			c.tempString = c.tempString .. word
		end
		--print("Current word: "..word)
		c.currentWord = c.currentWord + 1
	end
end

function catlang.put(stack, val)
	table.insert(c.stack[stack], val)
end

function catlang.getFromStack(stack, count)
	local tab = {}
	local is = #c.stack[stack]
	local ie = #c.stack[stack] - (count or 0)
	for i = is, ie, -1 do
		table.insert(tab, c.stack[stack][i])
		table.remove(c.stack[stack], #c.stack[stack])
	end
	return unpack(tab)
end


catlang.initialize()
io.write("Enter name of file (without extention): ") filename = io.read()
catlang.load((filename or "")..".catlang")

os.execute("cls")

function catlang.run()
	catlang.parse("MAIN")
end

catlang.run()